import math
inputnumber = '0'

# Functions
def calculator(semn, inputnumber, labelistoric):
    global lastValue
    global textforlabel
    if (semn == "+"):
        lastValue = lastValue + inputnumber
        textforlabel = textforlabel + "{}+".format(inputnumber)
        labelistoric.configure(text=textforlabel)
        return lastValue
    elif (semn == "-"):
        lastValue = lastValue - inputnumber
        textforlabel = textforlabel + "-{}".format(inputnumber)
        labelistoric.configure(text=textforlabel)
        return lastValue
    elif (semn == "*"):
        lastValue = lastValue * inputnumber
        textforlabel = textforlabel + "*{}".format(inputnumber)
        labelistoric.configure(text=textforlabel)
        return lastValue
    elif (semn == "/"):
        try:
            lastValue = lastValue / inputnumber
            textforlabel = textforlabel + "/{}".format(inputnumber)
            labelistoric.configure(text=textforlabel)
            return lastValue
        except ZeroDivisionError as error:
            textforlabel = ""
            labelistoric.configure(text=textforlabel)
            return error
    elif (semn == "√"):
        lastValue = math.sqrt(inputnumber)
        textforlabel = textforlabel + "√{}".format(inputnumber)
        labelistoric.configure(text=textforlabel)
        return lastValue
    elif (semn == "sin"):
        lastValue = math.sin(inputnumber)
        textforlabel = textforlabel + "sin({})".format(inputnumber)
        labelistoric.configure(text=textforlabel)
        return lastValue
    elif (semn == "cos"):
        lastValue = math.cos(inputnumber)
        textforlabel = textforlabel + "cos({})".format(inputnumber)
        labelistoric.configure(text=textforlabel)
        return lastValue
    elif (semn == "tg"):
        lastValue = math.sin(inputnumber)
        textforlabel = textforlabel + "tg({})".format(inputnumber)
        labelistoric.configure(text=textforlabel)
        return lastValue
    elif (semn == "ctg"):
        try:
            lastValue = 1 / math.tan(inputnumber)
            textforlabel = textforlabel + "ctg({})".format(inputnumber)
            labelistoric.configure(text=textforlabel)
            return lastValue
        except ZeroDivisionError as error:
            textforlabel = ""
            labelistoric.configure(text=textforlabel)
            return error
    elif (semn == "1/x"):
        lastValue = 1 / inputnumber
        textforlabel = ''
        labelistoric.configure(text="reciproc({})".format(inputnumber))
        return lastValue

def swichChar(char, label):
    global inputnumber
    swicher = {
        0: '0',
        1: '1',
        2: '2',
        3: '3',
        4: '4',
        5: '5',
        6: '6',
        7: '7',
        8: '8',
        9: '9'
    }
    inputnumber = inputnumber + swichChar(str(char))
    label.configure(text=inputnumber)

def setLabelNumber(number, label):
    global inputnumber
    if (number == '1'):
        inputnumber = inputnumber + "1"
        label.configure(text=inputnumber)
    elif (number == '2'):
        inputnumber = inputnumber + "2"
        label.configure(text=inputnumber)
    elif (number == '3'):
        inputnumber = inputnumber + "3"
        label.configure(text=inputnumber)
    elif (number == '4'):
        inputnumber = inputnumber + "4"
        label.configure(text=inputnumber)
    elif (number == '5'):
        inputnumber = inputnumber + "5"
        label.configure(text=inputnumber)
    elif (number == '6'):
        inputnumber = inputnumber + "6"
        label.configure(text=inputnumber)
    elif (number == '7'):
        inputnumber = inputnumber + "7"
        label.configure(text=inputnumber)
    elif (number == '8'):
        inputnumber = inputnumber + "8"
        label.configure(text=inputnumber)
    elif (number == '9'):
        inputnumber = inputnumber + "9"
        label.configure(text=inputnumber)
    elif (number == '0' and (not inputnumber == '')):
        inputnumber = inputnumber + "0"
        label.configure(text=inputnumber)
    elif (number == "."):
        if not(str(inputnumber).__contains__(".")):
            inputnumber = inputnumber + "."
            label.configure(text=inputnumber)
    elif (number == "←"):
        inputnumber = inputnumber[:-1]
        if(inputnumber == '' or inputnumber == '0'):
            label.configure(text="0")
        else:
            label.configure(text=inputnumber)

def reset(label, labelistoric):
    global lastValue
    global textforlabel
    global inputnumber
    lastValue = 0
    textforlabel = ""
    inputnumber = ""
    label.configure(text=lastValue)
    labelistoric.configure(text=textforlabel)
