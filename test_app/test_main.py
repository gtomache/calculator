from unittest import TestCase


class Test(TestCase):
    def test_calculator(self):
        from main import calculator
        self.assertTrue(calculator("+", 7.0, 3.0), 10.0)
        self.assertTrue(calculator("-", 7.0, 3.0), 4.0)
        self.assertTrue(calculator("*", 7.0, 3.0), 21.0)
        self.assertTrue(round(calculator("/", 7.0, 3.0), 2), round(2.333333))
        self.assertTrue(calculator("/", 0.0, 0.0), ZeroDivisionError)
