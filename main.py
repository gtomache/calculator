from tkinter import *

from functions import calculator, setLabelNumber, reset

lastValue = 0
textforlabel = ''
inputnumber = ''

def populatescreen(root):

    # Label
    labelistoric = Label(text="", font=("Courier", 8), justify=RIGHT)
    labelistoric.place(x=200, y=50)
    label = Label(text="0", font=("Courier", 10), justify=RIGHT)
    label.place(x=450, y=200)

    ###################----------Buttons----------###################################

    # = sign
    buttonegal = Button(root, text="=", width=5, height=5)
    buttonegal.place(x=450, y=400)

    # 1/x sign
    buttoninversa = Button(root, text="1/x", width=5, height=2, command=lambda: label.configure(
        text=calculator(("1/x", int(inputnumber), labelistoric))))
    buttoninversa.place(x=450, y=350)

    # % sign
    buttonlasuta = Button(root, text="%", width=5, height=2, command=lambda: label.configure(
        text=calculator(("%", int(inputnumber), labelistoric))))
    buttonlasuta.place(x=450, y=300)

    # √ sign
    buttonradical = Button(root, text="√", width=5, height=2, command=lambda: label.configure(
        text=calculator(("√", int(inputnumber), labelistoric))))
    buttonradical.place(x=450, y=250)

    # + sign
    buttonplus = Button(root, text="+", width=5, height=2, command=lambda: label.configure(
        text=calculator(("+", int(inputnumber), labelistoric))))
    buttonplus.place(x=400, y=445)

    # - sign
    buttonminus = Button(root, text="-", width=5, height=2, command=lambda: label.configure(
        text=calculator(("-", int(inputnumber), labelistoric))))
    buttonminus.place(x=400, y=400)

    # * sign
    buttonori = Button(root, text="*", width=5, height=2, command=lambda: label.configure(
        text=calculator(("*", int(inputnumber), labelistoric))))
    buttonori.place(x=400, y=350)

    # / sign
    buttonimpartit = Button(root, text="/", width=5, height=2, command=lambda: label.configure(
        text=calculator(("/", int(inputnumber), labelistoric))))
    buttonimpartit.place(x=400, y=300)

    # ± sign
    buttonplusminus = Button(root, text="±", width=5, height=2, command=lambda: label.configure(
        text=calculator(("±", int(inputnumber), labelistoric))))
    buttonplusminus.place(x=400, y=250)

    # . sign
    buttonpunct = Button(root, text=".", width=5, height=2, command=lambda: setLabelNumber(".", label))
    buttonpunct.place(x=350, y=445)
    # 3 sign
    buttontrei = Button(root, text="3", width=5, height=2, command=lambda: setLabelNumber("3", label))
    buttontrei.place(x=350, y=400)
    # 6 sign
    buttonsase = Button(root, text="6", width=5, height=2, command=lambda: setLabelNumber("6", label))
    buttonsase.place(x=350, y=350)
    # 9 sign
    buttonnoua = Button(root, text="9", width=5, height=2, command=lambda: setLabelNumber("9", label))
    buttonnoua.place(x=350, y=300)

    # C sign
    buttonc = Button(root, text="C", width=5, height=2, command=lambda: reset(label, labelistoric))
    buttonc.place(x=350, y=250)

    # 0 sign
    buttonzero = Button(root, text="0", width=12, height=2, command=lambda: setLabelNumber("0", label))
    buttonzero.place(x=250, y=445)
    # 2 sign
    buttondoi = Button(root, text="2", width=5, height=2, command=lambda: setLabelNumber("2", label))
    buttondoi.place(x=300, y=400)
    # 5 sign
    buttoncinci = Button(root, text="5", width=5, height=2, command=lambda: setLabelNumber("5", label))
    buttoncinci.place(x=300, y=350)
    # 8 sign
    buttonopt = Button(root, text="8", width=5, height=2, command=lambda: setLabelNumber("8", label))
    buttonopt.place(x=300, y=300)

    # CE sign
    buttonce = Button(root, text="CE", width=5, height=2, command=lambda: reset(label, labelistoric))
    buttonce.place(x=300, y=250)

    # 1 sign
    buttonunu = Button(root, text="1", width=5, height=2, command=lambda: setLabelNumber("1", label))
    buttonunu.place(x=250, y=400)

    # 4 sign
    buttonpatru = Button(root, text="4", width=5, height=2, command=lambda: setLabelNumber("4", label))
    buttonpatru.place(x=250, y=350)

    # 7 sign
    buttonsapte = Button(root, text="7", width=5, height=2, command=lambda: setLabelNumber("7", label))
    buttonsapte.place(x=250, y=300)

    # ← sign
    buttonbackspace = Button(root, text="←", width=5, height=2, command=lambda: setLabelNumber("←", label))
    buttonbackspace.place(x=250, y=250)

    # 10^x sign
    buttonzecelaputereax = Button(root, text="10^x", width=5, height=2, command=lambda: reset(label, labelistoric))
    buttonzecelaputereax.place(x=200, y=445)

    # 3√x sign
    buttonradordtreidinx = Button(root, text="3√x", width=5, height=2, command=lambda: label.configure(
        text=calculator(("3√x", int(inputnumber), labelistoric))))
    buttonradordtreidinx.place(x=200, y=400)

    # y√x sign
    buttonradydinx = Button(root, text="y√x", width=5, height=2, command=lambda: label.configure(
        text=calculator(("y√x", int(inputnumber), labelistoric))))
    buttonradydinx.place(x=200, y=350)

    # x! sign
    buttonxfactorial = Button(root, text="x!", width=5, height=2, command=lambda: label.configure(
        text=calculator(("x!", int(inputnumber), labelistoric))))
    buttonxfactorial.place(x=200, y=300)

    # ) sign
    buttonsfparanteza = Button(root, text=")", width=5, height=2, command=lambda: label.configure(
        text=calculator((")", int(inputnumber), labelistoric))))
    buttonsfparanteza.place(x=200, y=250)

    # log sign
    buttonlog = Button(root, text="log", width=5, height=2, command=lambda: reset(label, labelistoric))
    buttonlog.place(x=150, y=445)

    # x^3 sign
    buttonxlaputereatrei = Button(root, text="x^3", width=5, height=2, command=lambda: label.configure(
        text=calculator(("x^3", int(inputnumber), labelistoric))))
    buttonxlaputereatrei.place(x=150, y=400)

    # x^y sign
    buttonxlaputereay = Button(root, text="x^y", width=5, height=2, command=lambda: label.configure(
        text=calculator(("x^y", int(inputnumber), labelistoric))))
    buttonxlaputereay.place(x=150, y=350)

    # x^2 sign
    buttonxlaputereadoua = Button(root, text="x^2", width=5, height=2, command=lambda: label.configure(
        text=calculator(("x^2", int(inputnumber), labelistoric))))
    buttonxlaputereadoua.place(x=150, y=300)

    # ( sign
    buttoninceputparanteza = Button(root, text="(", width=5, height=2, command=lambda: label.configure(
        text=calculator(("(", int(inputnumber), labelistoric))))
    buttoninceputparanteza.place(x=150, y=250)

    # MOD sign
    buttonmod = Button(root, text="MOD", width=5, height=2, command=lambda: label.configure(
        text=calculator(("MOD", int(inputnumber), labelistoric))))
    buttonmod.place(x=100, y=445)

    # tg sign
    buttontg = Button(root, text="tg", width=5, height=2, command=lambda: label.configure(
        text=calculator(("tg", int(inputnumber), labelistoric))))
    buttontg.place(x=100, y=400)
    # cos sign
    buttoncos = Button(root, text="cos", width=5, height=2, command=lambda: label.configure(
        text=calculator(("cos", int(inputnumber), labelistoric))))
    buttoncos.place(x=100, y=350)

    # sin sign
    buttonsin = Button(root, text="sin", width=5, height=2, command=lambda: label.configure(
        text=calculator(("sin", int(inputnumber), labelistoric))))
    buttonsin.place(x=100, y=300)

    # ln sign
    buttonln = Button(root, text="ln", width=5, height=2, command=lambda: label.configure(
        text=calculator(("ln", int(inputnumber), labelistoric))))
    buttonln.place(x=100, y=250)



    # # ctg sign
    # buttonctg = Button(root, text="ctg", width=5, height=2, command=lambda: label.configure(
    #     text=calculator(("ctg", int(inputnumber), labelistoric)))
    # buttonctg.place(x=300, y=450)


if __name__ == '__main__':
    # Root and size of root
    root = Tk()
    root.geometry("500x500")

    # Establishing Label to root
    mylabel = Label(root, text="My Label")

    # Populate Screen
    populatescreen(root)

    root.mainloop()
